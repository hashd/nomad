# Nomad
## Markdown file viewer for the console

Opening a markdown file using a text editor can be a pain sometimes due to the overhead of opening another application. **Nomad** aims to aide you in reading markdown files directly from the console.

This is still a POC using marked-terminal.

### Installation
Install using npm

```Shell
npm install -g nomd
```

### Usage

```Shell
nmd $PATH_TO_FILE
(or)
nomd $PATH_TO_FILE
```

### Screenshots
![Nomad vs Cat](etc/nomad-cat.png)