#!/usr/bin/env node
if (process.argv.length < 3) {
	console.log('Usage: nmd $filename');
	process.exit(1);
}

var fs = require('fs'),
	filename = process.argv[2],
	marked = require('marked'),
	TerminalRenderer = require('marked-terminal'),
	chalk = require('chalk');

marked.setOptions({
	renderer: new TerminalRenderer({
		showSectionPrefix: true,
		code: chalk.red,
		codespan: chalk.cyan
	})
});

console.log(marked(fs.readFileSync(filename).toString()));